#ifndef _SYSTICK_H_
#define _SYSTICK_H_

#include "system.h"


//时钟源获取 配置
void SysTick_Init(u8 SYSCLK);

//微秒的延时
void delay_us(u32 nus);

//毫秒的延时
void delay_ms(u16 nms);

#endif


