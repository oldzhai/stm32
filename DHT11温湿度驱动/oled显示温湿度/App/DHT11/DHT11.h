#ifndef __DHT11_H
#define __DHT11_H	 

#include "system.h"

#define BOOL unsigned char
 
#ifndef TRUE
#define TRUE 1
#endif
 
#ifndef FALSE
#define FALSE 0
#endif


//定义DHT11 GPIOD 0
#define DHT11_PORT_RCC  	RCC_APB2Periph_GPIOD
#define DHT11_PIN  			GPIO_Pin_0
#define DHT11_PORT 			GPIOD

static void DHT11_DataPin_Configure_Output(void);
static void DHT11_DataPin_Configure_Input(void);
BOOL DHT11_get_databit(void);
void DHT11_set_databit(BOOL level);
void mdelay(u16 ms);
void udelay(u16 us);
static uint8_t DHT11_read_byte(void);
static uint8_t DHT11_start_sampling(void);
void DHT11_get_data(u32 *buf);

	 				    
#endif


