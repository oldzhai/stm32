//--------------------------------------------------------------------------------------
//说明: STM32F103Zet6 驱动DHT11 在 0.96 oled屏显示温湿度
//引脚说明
//	DHT11: PD0
//	OLED:	D0   接PC0（SCL） 	
//				D1   接PC1（SDA）
//				RES  接PC2  
//				DC   接PC3 	CS   接PA0	
//--------------------------------------------------------------------------------------

#include "system.h"
#include "SysTick.h"  //延时  
#include "oled.h"
#include "dht11.h"

void dht11_show(u32 *buf); //oled显示


int main()
{
	//显示温度和湿度的数组 元素0 为温度高 元素1 为温度低 
	//										 元素2 为湿度高 元素3 为湿度的低
	u32 DTH11_data[4]={0};
	//时钟源获取 配置
	SysTick_Init(72); //精确延时
	
	OLED_Init();
	OLED_ColorTurn(0);//0正常显示，1 反色显示
  OLED_DisplayTurn(0);//0正常显示 1 屏幕翻转显示

	while(1)
	{	
		DHT11_get_data(DTH11_data); //获取到温湿度
		dht11_show(DTH11_data);
		delay_ms(5000);
		OLED_ShowString(70,20,"     ",16,1);
		OLED_ShowString(70,40,"     ",16,1);
		OLED_Refresh();
	}
}



//温湿度显示
void dht11_show(u32 *buf)
{
	u32 pos = 0;
	
	OLED_ShowChinese(20,20,0,16,1);//温
	OLED_ShowChinese(40,20,1,16,1);//度
	OLED_ShowString(60,20,":",16,1);
	
	if(buf[0] < 10)
	{
		OLED_ShowNum(70,20,buf[0],1,16,1);
		pos = 8;
	}
	else if(buf[0] < 100)
	{
		OLED_ShowNum(70,20,buf[0],2,16,1);
		pos = 16;
	}
	else
	{
		OLED_ShowNum(70,20,buf[0],3,16,1);
		pos = 24;
	}
	OLED_ShowString(70+pos,20,".",16,1);
	pos += 8;
	if(buf[1] < 10)
	{
		OLED_ShowNum(70+pos,20,buf[1],1,16,1);
	}
	else if(buf[1] < 100)
	{
		OLED_ShowNum(70+pos,20,buf[1],2,16,1);
	}
	else
	{
		OLED_ShowNum(70+pos,20,buf[1],3,16,1);
	}
	
		
	OLED_ShowChinese(20,40,2,16,1);//湿
	OLED_ShowChinese(40,40,3,16,1);//度
	OLED_ShowString(60,40,":",16,1);
	
	if(buf[2] < 10)
	{
		OLED_ShowNum(70,40,buf[2],1,16,1);
		pos = 8;
	}
	else if(buf[2] < 100)
	{
		OLED_ShowNum(70,40,buf[2],2,16,1);
		pos = 16;
	}
	else
	{
		OLED_ShowNum(70,40,buf[2],3,16,1);
		pos = 24;
	}
	OLED_ShowString(70+pos,40,".",16,1);
	pos += 8;
	if(buf[3] < 10)
	{
		OLED_ShowNum(70+pos,40,buf[3],1,16,1);
	}
	else if(buf[3] < 100)
	{
		OLED_ShowNum(70+pos,40,buf[3],2,16,1);
	}
	else
	{
		OLED_ShowNum(70+pos,40,buf[3],3,16,1);
	}
	OLED_Refresh();

}


