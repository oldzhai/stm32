//--------------------------------------------------------------------------------------
//说明: STM32F103Zet6 驱动 0.96 oled屏128x64	  
//spi 四线接法
//              GND    电源地
//              VCC  接5V或3.3v电源
//              D0   接PA4（SCL）时钟
//              D1   接PA5（SDA）数据
//              RES  接PA2 复位
//              DC   接PA6 标志位，判断是命令还是数据
//              CS   接PA7  片选（拉低有效）
//                              ***注意***
//特别注意：当我们修修改完代码发现屏幕不显示，在通电的情况下
//      代码运行错误时,屏幕是黑暗的，我们可以通过点灯来判断程序是否正常运行
//大多情况是代码运行错误卡住了从而导致屏幕不显示（保证接线没问题----除了电
//源其他管脚可自己随便定义）
//--------------------------------------------------------------------------------------

#include "system.h"
#include "SysTick.h"  //延时  
#include "oled.h"
	
int main()
{
	//时钟源获取 配置
	SysTick_Init(72); //精确延时
	
	OLED_Init();
	OLED_ColorTurn(0);//0正常显示，1 反色显示
  OLED_DisplayTurn(0);//0正常显示 1 屏幕翻转显示

	//显示汉字
	while(1)
	{
		OLED_ShowChinese(0,0,13,16,1);//博
		OLED_ShowChinese(20,0,14,16,1);//客
		OLED_ShowString(40,0,":",16,1);
		OLED_ShowString(0,18,"oldzhai.gitee.io",16,1);
		
		OLED_ShowString(25,45,"I",16,1);
		OLED_ShowString(40,45,"T",16,1);
		OLED_ShowChinese(60,45,11,16,1);//老
		OLED_ShowChinese(80,45,12,16,1);//翟
		OLED_Refresh();//更新到oled屏上
	}
}



