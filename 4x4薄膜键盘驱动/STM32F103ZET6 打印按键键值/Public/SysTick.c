#include "SysTick.h"

static u8  fac_us = 0;
static u16 fac_ms = 0;

//时钟源获取 （系统时钟）
void SysTick_Init(u8 SYSCLK)
{
	//SysTick 时钟源的获取（系统时钟 8分频）
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
	fac_us = SYSCLK / 8;           //获取微秒的次数
	fac_ms = (u16)fac_us * 1000;   //获取毫秒的次数
	

}


//微秒的延时
void delay_us(u32 nus)
{
	u32 temp;
	SysTick->LOAD = nus * fac_us;  //时间加载
	SysTick->VAL = 0x00;    //清空计数器
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk; //开始倒数
	do
	{
		temp = SysTick->CTRL;
	}while((temp & 0x01) && !(temp &(1<<16)));  //等待时间到达 
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk; //关闭计数器
	SysTick->VAL = 0x00;   //清空计数器
	
}
	
	
//毫秒的延时  最大不能超过 1864（24为 FFFFFF / 9000）
void delay_ms(u16 nms)
{
	u32 temp;
	SysTick->LOAD = (u32)nms * fac_ms;  //时间加载 LOAD为24bit
	SysTick->VAL = 0x00;    //清空计数器
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk; //开始倒数
	do
	{
		temp = SysTick->CTRL;
	}while((temp & 0x01) && !(temp &(1<<16)));  //等待时间到达
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk; //关闭计数器
	SysTick->VAL = 0x00;   //清空计数器
	
}
	

