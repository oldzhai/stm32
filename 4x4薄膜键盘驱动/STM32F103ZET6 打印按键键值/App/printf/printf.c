#include "printf.h"

int fputc(int ch,FILE *p)
{
	USART_SendData(USART1,(u8)ch);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == Bit_RESET);
	return ch;
}


void printf_init()
{

	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure; //定义一个串口结构体变量
	NVIC_InitTypeDef NVIC_InitStructure; //中断优先级结构体
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE); //打开时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE); //打开管脚复用时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE); //打开串口时钟(挂载在APB2总线上的)
	
	//配置管脚
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //TX 发送
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用管脚
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //时钟频率
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	//配置管脚
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; //RX 接收
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; //浮空输入
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	
	//配置串口1结构体
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl =
	USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_Init(USART1, &USART_InitStructure);

	USART_Cmd(USART1, ENABLE); //使能串口1
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); //串口1的接收中断使能
	USART_ClearFlag(USART1,USART_FLAG_TC); //清空标志位(发送完成)
	
	//中断优先级 NVIC参数的配置
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn; //打开EXTI2的全局中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; //抢占优先级为0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; //响应优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //使能
	NVIC_Init(&NVIC_InitStructure); //初始化化

}

