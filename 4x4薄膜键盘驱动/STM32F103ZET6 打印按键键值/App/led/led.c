#include "led.h"

void LED_Init(void)
{
	//配置时钟
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(LED_PORT_RCC,ENABLE);
	
	//配置管脚
	GPIO_InitStructure.GPIO_Pin = LED_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //时钟频率
	GPIO_Init(LED_PORT,&GPIO_InitStructure);
	
	GPIO_SetBits(LED_PORT,LED_PIN); //设置高电平


}


