#ifndef __KEY_H__
#define	__KEY_H__
#include "stm32f10x.h"
#include "system.h"
#include "SysTick.h"

#define		ROW_1		PCout(6)
#define		ROW_2		PCout(7)
#define		ROW_3		PCout(8)
#define		ROW_4		PCout(9)
#define		COL_1		PAout(4)
#define		COL_2		PAout(5)
#define		COL_3		PAout(6)
#define		COL_4		PAout(7)
#define		ROW1		GPIO_Pin_6
#define		ROW2		GPIO_Pin_7
#define		ROW3		GPIO_Pin_8
#define		ROW4		GPIO_Pin_9
#define		COL1		GPIO_Pin_4
#define		COL2		GPIO_Pin_5
#define		COL3		GPIO_Pin_6
#define		COL4		GPIO_Pin_7

typedef enum
{
	Column,
	Row,
	Normal
} MatirxMode;

extern void MatrixKeyConfiguration(void);
extern uint8_t GetMatrixKeyValue(void);
extern uint8_t GetMatrixKeyValue_Once(void);

#endif

