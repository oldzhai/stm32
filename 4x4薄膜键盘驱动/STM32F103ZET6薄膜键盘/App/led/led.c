#include "led.h"

//用PA2 管脚控制led灯的点亮

void LED_Init(void)
{
	//定义结构体变量
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(LED_PORT_RCC,ENABLE); //开启端口时钟
	
	//配置管脚
	GPIO_InitStructure.GPIO_Pin = LED_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //时钟频率
	GPIO_Init(LED_PORT,&GPIO_InitStructure);
	
	GPIO_SetBits(LED_PORT,LED_PIN); //设置高电平
}


