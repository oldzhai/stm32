#ifndef _LED_H
#define _LED_H

#include "system.h"

#define LED_PORT_RCC   	RCC_APB2Periph_GPIOA
#define LED_PIN       	GPIO_Pin_2
#define LED_PORT     		GPIOA

void LED_Init(void);

#define LED  PAout(2)


#endif


