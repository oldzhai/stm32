/**********************************************************************
STM32F103Zet6驱动4x4薄膜键盘 这里没有就简单的用led灯来测试
	八个引脚的连接方式
	行：	ROW_1		PC(6)  5
				ROW_2		PC(7)  6
				ROW_3		PC(8)  7
				ROW_4		PC(9)  8
  
	列：	COL_1		PA(4)  1
				COL_2		PA(5)  2
				COL_3		PA(6)  3
				COL_4		PA(7)  4
	管脚根据自己的板子可自定义 （薄膜键盘1-4 为列 5-8 为行 主要看连接处有编号）
***********************************************************************/


#include "system.h"
#include "led.h"
#include "SysTick.h"
#include "key.h"
	
int main()
{
	
	u8 key;

	//时钟源获取 配置
	SysTick_Init(72);
	
	//led 端口配置
	LED_Init();
	LED = 0;
	delay_ms(1000);
	LED = 1;

	while(1)
	{
		key = GetMatrixKeyValue();
		switch (key)
		{
			case 1:
				LED = 0;
				break;
			case 2:
				LED = 1;
				break;
			case 3:
				LED = 0;
				break;
			case 4:
				LED = 1;
				break;
		
		}
	}
		
}

	




